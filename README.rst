.. contents:: Table of Contents

========
Overview
========

SPECS Parent is a maven project containing a top-level parent pom.xml file which serves as a parent project for all SPECS Java-based components. The pom.xml file contains common project configuration inherited by other SPECS components.

=====
Usage
=====

To make the SPECS Parent the parent of your Maven-based project, add the following to the pom.xml file of your project:

.. code-block:: xml

 <parent>
    <groupId>eu.specs-project.utility</groupId>
    <artifactId>specs-parent</artifactId>
    <version>0.1-SNAPSHOT</version>
    <relativePath/>
 </parent>

To enable Maven to find the SPECS Parent project and download it from the SPECS Maven repository, add the following ``specs`` profile to the ``<profiles>`` section of your Maven user ``settings.xml`` file which is located at::

 ${user.home}/.m2/settings.xml

.. code-block:: xml

 <profile> 
    <id>specs</id> 
    <activation> 
       <activeByDefault>true</activeByDefault> 
    </activation> 
    <repositories> 
       <repository> 
          <id>specs-snapshots</id>
          <name>SPECS Snapshots Repository</name>
          <url>https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots</url>
          <layout>default</layout>
          <releases>
             <enabled>false</enabled>
          </releases>
          <snapshots>
             <enabled>true</enabled>
             <updatePolicy>always</updatePolicy>
          </snapshots>
       </repository> 
    </repositories> 
 </profile> 

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Parent is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Parent is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.